//
//  CustomTextField.swift
//  SwiftUIChatTutorial
//
//  Created by 정주리 on 2022/02/01.
//

import SwiftUI

struct CustomTextField: View {
    let imageName: String
    let placeholderText: String
    let isSecureField: Bool
    @Binding var text: String
    
    var body: some View {
        HStack(spacing: 16) {
            Image(systemName: imageName)
                .resizable()
                .scaledToFit()
                .frame(width: 20, height: 20)
                .foregroundColor(Color(.darkGray))
            
            if isSecureField {
                SecureField(placeholderText, text: $text)
            } else {
                TextField(placeholderText, text: $text)
            }
        }
        
        Divider()
            .background(Color(.darkGray))
    }
}
