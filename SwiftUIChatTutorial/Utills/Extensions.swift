//
//  Extensions.swift
//  SwiftUIChatTutorial
//
//  Created by 정주리 on 2022/03/16.
//

import UIKit

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder),
                   to: nil,
                   from: nil,
                   for: nil)
    }
}
