//
//  SettingsCellViewModel.swift
//  SwiftUIChatTutorial
//
//  Created by 정주리 on 2022/02/01.
//
import SwiftUI

enum SettingsCellViewModel: Int, CaseIterable {
    case account
    case notifications
    case starredMessages
    //case privacy
    
    var imageName: String {
        switch self {
        case .account:
            return "key.fill"
        case .notifications:
            return "bell.badge.fill"
        case .starredMessages:
            return "star.fill"
        }
    }
    
    var title: String {
        switch self {
        case .account:
            return "Account"
        case .notifications:
            return "Notifications"
        case .starredMessages:
            return "Starred Messages"
        }
    }
    
    var backgroundColor: Color {
        switch self {
        case .account:
            return .blue
        case .notifications:
            return .red
        case .starredMessages:
            return .yellow
        }
    }
    
    
}
