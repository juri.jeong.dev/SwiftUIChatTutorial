//
//  NewMessageViewModel.swift
//  SwiftUIChatTutorial
//
//  Created by 정주리 on 2022/03/24.
//

import SwiftUI
import Firebase

class NewMessageViewModel: ObservableObject {
    @Published var users = [User]()
    
    init() {
        fetchUsers()
    }
    
    func fetchUsers() {
        COLLECTION_USERS.getDocuments { snapshot, _ in
            guard let documents = snapshot?.documents else { return }
            
            // way 1
            self.users = documents.compactMap({ try? $0.data(as: User.self) })
                .filter({ $0.id != Auth.auth().currentUser?.uid })
            print("DEBUG: Users \(self.users)")
            
            // way 2
            //documents.forEach { documents in
            //    guard let user = try? documents.data(as: User.self) else { return }
            //    self.users.append(user)
            //}
        }
    }
}
