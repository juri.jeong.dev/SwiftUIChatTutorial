//
//  AuthViewModel.swift
//  SwiftUIChatTutorial
//
//  Created by 정주리 on 2022/03/21.
//

import Firebase
import UIKit
import FirebaseFirestore

class AuthViewModel: NSObject, ObservableObject {
    @Published var didAuthenticateUser = false
    @Published var userSession: FirebaseAuth.User? // keeping user looged in
    @Published var currentUser: User? // populating settings view with user. just user created
    private var temCurrentUser: FirebaseAuth.User? // part of firebase libraries
    
    static let shared = AuthViewModel()
    
    override init() {
        super.init()
        
        print("DEBUG: Auth view model did init..")
        userSession = Auth.auth().currentUser
        
        fetchUser()
    }
    
    func login(withEmail email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            if let error = error {
                print("DEBUG: Failed to sign in with error \(error.localizedDescription)")
                return
            }
            
            self.userSession = result?.user
            self.fetchUser()
        }
    }
    
    func register(withEmail email: String,
                  password: String,
                  fullname: String,
                  username: String) {
        Auth.auth().createUser(withEmail: email, password: password) { result, error in
            if let error = error {
                print("DEBUG: Failed to register with error \(error.localizedDescription)")
                return
            }
            
            guard let user = result?.user else { return }
            self.temCurrentUser = user
            
            let data: [String: Any] = ["email": email,
                                       "username": username,
                                       "fullname": fullname]
            COLLECTION_USERS.document(user.uid).setData(data) { _ in
                self.didAuthenticateUser = true
            }
        }

    }
    
    func uploadProfileImage(_ image: UIImage) {
        print("DEBUG: Upload profile image from view model...")
        guard let uid = temCurrentUser?.uid else {
            print("DEBUG: Failed to set temp current user...")
            return
        }
        
        ImageUploader.uploadImage(image: image) { imageUrl in
            COLLECTION_USERS.document(uid).updateData(["profileImageUrl": imageUrl]) { _ in
                print("DEBUG: Successfully update user data...")
                self.userSession = self.temCurrentUser
            }
        }
        
    }
    
    func signout() {
        self.userSession = nil
        try? Auth.auth().signOut()
    }
    
    func fetchUser() {
        guard let uid = userSession?.uid else { return }
        
        COLLECTION_USERS.document(uid).getDocument { snapshot, _ in
            guard let user = try? snapshot?.data(as: User.self) else { return }
            self.currentUser = user
        }
        
        // old way
        
        //docRef.getDocument { (document, _) in
        //    //guard let document = document else {
        //    //    print("Document does not exist")
        //    //    return
        //    //}
        //    //let dataDescription = document.data().map(String.init(describing: ))
        //    //print("Document data: \(String(describing: dataDescription))")
        //}
        
    }
}
