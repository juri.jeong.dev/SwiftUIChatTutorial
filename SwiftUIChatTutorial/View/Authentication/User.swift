//
//  User.swift
//  SwiftUIChatTutorial
//
//  Created by 정주리 on 2022/03/24.
//

import FirebaseFirestoreSwift
import Foundation

struct User: Identifiable, Decodable {
    @DocumentID var id: String?
    let username: String
    let fullname: String
    let email: String
    let profileImageUrl: String
}

let MOCK_USER = User(id: "21413242352363",
                     username: "juri",
                     fullname: "juri jeong",
                     email: "juri@gmail.com",
                     profileImageUrl: "www.google.com")
