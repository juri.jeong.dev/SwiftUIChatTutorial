//
//  ConversationsView.swift
//  SwiftUIChatTutorial
//
//  Created by 정주리 on 2022/02/01.
//

import SwiftUI

struct ConversationsView: View {
    @State private var showNewMessageView = false
    @State private var showChatView = false
    @State var selecetedUser: User?
    @ObservedObject var viewModel = ConversationViewModel()
    
    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            
            if let user = selecetedUser {
                NavigationLink(destination: ChatView(user: user),
                               isActive: $showChatView,
                               label: { })
            }
            
            
            // chats
            ScrollView {
                VStack(alignment: .leading) {
                    
                    ForEach(viewModel.recentMessages) { message in
                        NavigationLink(
                            destination: ChatView(user: MOCK_USER),
                            label: { ConversationCell(viewModel: MessageViewModel(message: message)) }
                        )
                    }
                }
            }
            
            // floating button
            Button(action: {
                showNewMessageView.toggle()
            }, label: {
                Image(systemName: "square.and.pencil")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 24, height: 24)
                    .padding()
            })
            .background(Color(.systemBlue))
            .foregroundColor(.white)
            .clipShape(Circle())
            .padding()
            .fullScreenCover(isPresented: $showNewMessageView,
                             content: { NewMessageView(showChatView: $showChatView, user: $selecetedUser)
            })
        }
    }
}

struct ConversationsView_Previews: PreviewProvider {
    static var previews: some View {
        ConversationsView()
    }
}
