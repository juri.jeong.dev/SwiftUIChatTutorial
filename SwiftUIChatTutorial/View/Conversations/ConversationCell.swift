//
//  UserCell.swift
//  SwiftUIChatTutorial
//
//  Created by 정주리 on 2022/03/16.
//

import SwiftUI

struct ConversationCell: View {
    let viewModel: MessageViewModel
    
    var body: some View {
        VStack {
            HStack {
                
                Image("doctor")
                    .resizable()
                    .scaledToFill()
                    .frame(width: 48, height: 48)
                    .clipShape(Circle())

                //message info
                VStack(alignment: .leading, spacing: 4) {
                    Text("Stephen Vincent Strange")
                        .font(.system(size: 14, weight: .semibold))

                    Text(viewModel.message.text)
                        .font(.system(size: 15))
                }
                .foregroundColor(.black)
                
                Spacer()
            }
            .padding(.horizontal)
            
            Divider()
        }
        .padding(.top)
    }
}
