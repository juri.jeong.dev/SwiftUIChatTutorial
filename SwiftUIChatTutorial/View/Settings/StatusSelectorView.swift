//
//  StatusSelctorView.swift
//  SwiftUIChatTutorial
//
//  Created by 정주리 on 2022/03/10.
//

import SwiftUI

struct StatusSelectorView: View {
    @ObservedObject var viewModel = StatusViewModel()
    @State var status: UserStatus = .notConfigured
    
    var body: some View {
        ZStack {
            Color(.systemGroupedBackground)
                .ignoresSafeArea()

            ScrollView {
                VStack(alignment: .leading) {
                    Text("CURRENTYL SET TO")
                        .foregroundColor(.gray)
                        .padding()

                    StatusCell(status: viewModel.status)

                    Text("SELECT YOUR STATUS")
                        .foregroundColor(.gray)
                        .padding()

                    // for loop with options

                    ForEach(UserStatus.allCases.filter({
                        $0 != .notConfigured
                    }), id: \.self) { status in
                        Button(action: {
                            viewModel.updateStatus(status)
                        }, label: {
                            StatusCell(status: status)
                        })

                    }
                }
            }
        }
    }
}

struct StatusSelctorView_Previews: PreviewProvider {
    static var previews: some View {
        StatusSelectorView()
    }
}

struct StatusCell: View {
    let status: UserStatus
    
    var body: some View {
        HStack {
            Text(status.title)
                .foregroundColor(.black)
            Spacer()
        }
        .frame(height: 56)
        .padding(.horizontal)
        .background(Color.white)
    }
}
