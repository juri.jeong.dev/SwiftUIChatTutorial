//
//  ChannelsView.swift
//  SwiftUIChatTutorial
//
//  Created by 정주리 on 2022/02/01.
//

import SwiftUI

struct ChannelsView: View {
    var body: some View {
        Text("Channels view goes here..")
    }
}

struct ChannelsView_Previews: PreviewProvider {
    static var previews: some View {
        ChannelsView()
    }
}
