//
//  Message.swift
//  SwiftUIChatTutorial
//
//  Created by 정주리 on 2022/03/21.
//

import Firebase
import FirebaseFirestoreSwift

struct Message: Identifiable, Decodable {
    @DocumentID var id: String?
    let fromId: String
    let toId: String
    let read: Bool
    let text: String
    let timestamp: Timestamp
    
    var user: User?
}
